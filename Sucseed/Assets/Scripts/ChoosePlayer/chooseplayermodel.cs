﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class chooseplayermodel : MonoBehaviour
{

    GameObject emoel;
    GameObject samuel;
    public GameObject Part;
    public GameObject canv;
    public GameObject Samuel;
    public GameObject Emoel;


    void Start()
    {
        emoel = Resources.Load<GameObject>("Prefabs/Emoel");
        emoel = Instantiate(emoel);
        emoel.name = "Emoel";
        emoel.AddComponent<BoxCollider>().size = new Vector3(1.2f, 2.49f, 0.29f);
        emoel.GetComponent<BoxCollider>().center = new Vector3(0f, 1.2f, 0);
        emoel.transform.position = new Vector3(2, 0, 0);
        emoel.transform.eulerAngles = new Vector3(0, 180, 0);


        samuel = Resources.Load<GameObject>("Prefabs/Samuel");
        samuel = Instantiate(samuel);
        samuel.name = "Samuel";
        samuel.AddComponent<BoxCollider>().size = new Vector3(0.78f, 2.63f, 0.38f);
        samuel.GetComponent<BoxCollider>().center = new Vector3(0.02f, 1.31f, -0.05f);
        samuel.transform.position = new Vector3(0, 0, 0);
        samuel.transform.eulerAngles = new Vector3(0, 180, 0);
        samuel.transform.localScale = new Vector3(1, 1, 1);
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.name == "Emoel")
                {
                    Emoel.SetActive(true);
                    Part.SetActive(true);
                    Part.transform.position = new Vector3(-0.11f, 0, 6.94f);
                    canv.SetActive(true);
                    Samuel.SetActive(false);
                }

                if (hit.transform.name == "Samuel")
                {
                    Part.SetActive(true);
                    Part.transform.position = new Vector3(-2.11f, 0, 6.94f);
                    canv.SetActive(true);
                    Samuel.SetActive(true);
                    Emoel.SetActive(false);

                }
            }

        }
    }

    [System.Obsolete]
    public void ChooseEmoel()
    {
        if (Part.transform.position == new Vector3(-0.11f, 0, 6.94f))
        {
            Debug.Log("Emoel wurde gewählt");
            PlayerData playerdata = new PlayerData();
            playerdata.name = "Emoel";
            string emoel = JsonUtility.ToJson(playerdata);
            File.WriteAllText(Application.dataPath + "/SaveFiles/ChoosenPlayer.json", emoel);
            SceneManager.LoadScene("MainScene");
        }
    }

    public void EscapeChooseEmoel()
    {
        Part.SetActive(false);
        canv.SetActive(false);
        Emoel.SetActive(false);
        Samuel.SetActive(false);
    }

    [System.Obsolete]
    public void ChooseSamuel()
    {

        if (Part.transform.position == new Vector3(-2.11f, 0, 6.94f))
        {
            Debug.Log("Samuel wurde gewählt");
            PlayerData playerdata = new PlayerData();
            playerdata.name = "Samuel";
            string samuel = JsonUtility.ToJson(playerdata);
            File.WriteAllText(Application.dataPath + "/SaveFiles/ChoosenPlayer.json", samuel);
            SceneManager.LoadScene("MainScene");

        }
    }
    public void EscapeChooseSamuel()
    {
        Part.SetActive(false);
        canv.SetActive(false);
        samuel.SetActive(false);
        Emoel.SetActive(false);
    }


}

