﻿using UnityEngine;
    public enum CursorStates
    {
        Normal,Hoe,WateringCan,Shovel,Seed,Sickle
    }
/// <summary>
/// Made by Hannes Bauschke
/// </summary>
public class CursorScript : MonoBehaviour {
    public static CursorStates cursorState;
    public Texture2D Hoe,WateringCan,Shovel,Seed,Sickle;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public static GameObject cursorScript;
    public GameObject csHeader;
    private void Awake()
    {
        cursorScript = this.gameObject;
        csHeader = this.gameObject;
        
    }
    void Start () {
        cursorState = CursorStates.Normal;
    }
	
	void Update () {
        switch (cursorState)
        {
            case CursorStates.Normal:
                Cursor.SetCursor(null, Vector2.zero, cursorMode);
                break;
            case CursorStates.Hoe:
                Cursor.SetCursor(Hoe, hotSpot, cursorMode);
                break;
            case CursorStates.WateringCan:
                Cursor.SetCursor(WateringCan, hotSpot, cursorMode);
                break;
            case CursorStates.Shovel:
                Cursor.SetCursor(Shovel, hotSpot, cursorMode);
                break;
            case CursorStates.Seed:
                Cursor.SetCursor(Seed, hotSpot, cursorMode);
                break;
            case CursorStates.Sickle:
                Cursor.SetCursor(Sickle, hotSpot, cursorMode);
                break;
            default:
                break;
        }
    }
    public void SetCursorToNothing()
    {
        cursorState = CursorStates.Normal;
    }
    public void SetCursorToHoe()
    {
        cursorState = CursorStates.Hoe;
    }
    public void SetCursorToWatteringCan()
    {
        cursorState = CursorStates.WateringCan;
    }
    public void SetCursorToShovel()
    {
        cursorState = CursorStates.Shovel;
    }
    public void SetCursorToSeed()
    {
        cursorState = CursorStates.Seed;
    }
    public void SetCursorToSickle()
    {
        cursorState = CursorStates.Sickle;
    }
}
