﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScript : MonoBehaviour
{
    private GameObject cloud;
    private List<GameObject> allClouds = new List<GameObject>();

    void Awake()
    {
        cloud = (GameObject)Resources.Load("Prefabs/Cloud");

        StartCoroutine(SpawnClouds());
        StartCoroutine(MoveClouds());
    }

    private IEnumerator SpawnClouds()
    {
        GameObject lastSpawnedCloud = null;

        while (true)
        {
            float randomTime = Random.Range(0, 30);
            yield return new WaitForSeconds(randomTime);
            GameObject newCloud = Instantiate(cloud);

            if (lastSpawnedCloud != null)
            {
                while (true)
                {
                    float randomXValue = Random.Range(-150, 160);                                   //Range of values where the clouds can spawn without flashing and not collide with other clouds

                    if (lastSpawnedCloud.transform.position.x > randomXValue + 8 || lastSpawnedCloud.transform.position.x < randomXValue - 8)
                    {
                        newCloud.transform.position = new Vector3(randomXValue, -32, -10);
                        break;
                    }
                }
            }
            else
            {
                newCloud.transform.position = new Vector3(Random.Range(-150, 160), -32, -10);          //Range of values where the clouds can spawn without flashing
            }
            allClouds.Add(newCloud);
            lastSpawnedCloud = newCloud;
        }
    }

    private IEnumerator MoveClouds()
    {
        while (true)
        {
            List<GameObject> cleanUpClouds = new List<GameObject>();

            foreach (GameObject item in allClouds)
            {
                cloud = item;
                item.transform.LookAt(GameObject.Find("Main Camera").transform.position);
                item.transform.Translate(Vector3.forward * 2 * Time.deltaTime, Space.World);

                if (item.transform.position.z > 200)
                {
                    cleanUpClouds.Add(item);
                }
            }

            foreach (var item in cleanUpClouds)
            {
                if (allClouds.Contains(item))
                {
                    allClouds.Remove(item);
                    Destroy(item, 0.2f);
                }
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
