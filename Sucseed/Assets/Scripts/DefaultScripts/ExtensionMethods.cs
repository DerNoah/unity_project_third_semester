﻿//Noah Plützer
using UnityEngine;

namespace ExtensionMethods
{
    public static class PlantExtansions
    {
        const string resourcesPathToSeedGameObject = "Prefabs/Seed";
        const string resourcesPathToCuttingGameObject = "Prefabs/Cutting";



        public static void CheckPlant(this Plant plant)
        {
                Object.Destroy(plant.plantGameObject);

                switch (plant.stateOfPlant)
                {
                    case PlantState.Seed:

                        plant.plantGameObject = Object.Instantiate(Resources.Load<GameObject>(plant.pathToSpecific_S_Size));
                        plant.plantGameObject.transform.position = plant.scenePositionOfPlant;
                        plant.plantGameObject.AddComponent<CoroutineStarter>();
                        plant.plantGameObject.GetComponent<CoroutineStarter>().plantOfThisGameObject = plant;
                        break;
                    case PlantState.Cutting:
                        plant.plantGameObject = Object.Instantiate(Resources.Load<GameObject>(plant.pathToSpecific_M_Size));
                        plant.plantGameObject.transform.position = plant.scenePositionOfPlant;
                        plant.plantGameObject.AddComponent<CoroutineStarter>();
                        plant.plantGameObject.GetComponent<CoroutineStarter>().plantOfThisGameObject = plant;
                        break;
                    case PlantState.Flower:
                        plant.plantGameObject = Object.Instantiate(Resources.Load<GameObject>(plant.pathToSpecific_L_Size));
                        plant.plantGameObject.transform.position = plant.scenePositionOfPlant;
                        break;
                }
            //}


            plant.scenePositionOfPlant = new Vector3((FieldGrid.fields[plant.fieldNumber[0], plant.fieldNumber[1]].fieldObject.transform.position.x),              // position in Unity Scene x
                                                     (FieldGrid.fields[plant.fieldNumber[0], plant.fieldNumber[1]].fieldObject.transform.position.y) +             //}
                                                     (FieldGrid.fields[plant.fieldNumber[0], plant.fieldNumber[1]].fieldObject.transform.localScale.y / 2) +       // position in Unity Scene y
                                                     (FieldGrid.fields[plant.fieldNumber[0], plant.fieldNumber[1]].fieldObject.transform.localScale.y / 2),        //}
                                                     (FieldGrid.fields[plant.fieldNumber[0], plant.fieldNumber[1]].fieldObject.transform.position.z));             // position in Unity Scene z
        }

        public static void Grow(this Plant plant)
        {
            if (plant.stateOfPlant == PlantState.Seed)
            {
                plant.stateOfPlant = PlantState.Cutting;
            }
            else
            {
                plant.stateOfPlant = PlantState.Flower;
            }
        }
    }

    public static class FieldExtansions
    {
        public static void Hook(this Field field)
        {
            if (field.state == StateOfField.Watered)
            {
                field.state = StateOfField.WateredAndHooked;
            }
            else if (field.state == StateOfField.WateredAndHooked)
            {
                return;
            }
            else
            {
                field.state = StateOfField.Hooked;
            }
            field.Check();
        }
        public static void Water(this Field field)
        {
            if (field.state == StateOfField.Hooked)
            {
                field.state = StateOfField.WateredAndHooked;
            }
            else if (field.state == StateOfField.WateredAndHooked)
            {
                return;
            }
            else
            {
                field.state = StateOfField.Watered;
            }
            field.Check();
        }
        public static void Shovel(this Field field)
        {
            if (field.state == StateOfField.Watered || field.state == StateOfField.WateredAndHooked)
            {
                field.state = StateOfField.Watered;
            }
            else
            {
                field.state = StateOfField.Default;
            }
            field.Check();
        }
        public static void Sow(this Field field, Player player)
        {
            if (field.plantOnField == null)
            {
                if (field.state == StateOfField.Hooked || field.state == StateOfField.WateredAndHooked)
                {
                    if (player.heltedSeed[0].plantType == typeof(EmptySlot))
                    {
                        player.heltedSeed[0].parent.GetComponent<Slot>().Wegwerfen();
                        player.heltedSeed.Remove(player.heltedSeed[0]);
                        return;
                    }

                    field.plantOnField = (Plant)System.Activator.CreateInstance(player.heltedSeed[0].plantType, field.fieldNumber);
                    field.plantOnField.CheckPlant();
                    player.heltedSeed[0].parent.GetComponent<Slot>().Wegwerfen();
                    player.heltedSeed.Remove(player.heltedSeed[0]);

                    field.Check();
                }
                else
                {
                    //Do something
                    Debug.LogError("Field is not hooked");
                }
            }
        }
        public static void Harvest(this Field field)
        {
            if (field.plantOnField != null)
            {
                if (field.plantOnField.stateOfPlant == PlantState.Flower)
                {
                    Inventur.Inventar.AddItemToInv(2, field.plantOnField.thisPlantsItem);
                    field.state = StateOfField.Default;
                    field.plantOnField.CheckPlant();
                    field.Check();
                }
                else
                {
                    //Do something
                    Debug.LogError("Plant is not ripe");
                }
            }
        }
    }
}
