﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectsUpDown : MonoBehaviour
{
    Vector3 startPos;
    Vector3 heightPoint;
    public float speed;
    public float maxHeight;


    void Awake()
    {
        startPos = this.transform.position;
        heightPoint = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + maxHeight, gameObject.transform.position.z);

        StartCoroutine(SmoothUpDown(this.gameObject));
    }


    private IEnumerator SmoothUpDown(GameObject gameObject)
    {
        bool movesUp = true;
        Vector3 velocity = Vector3.zero;

        while (true)
        {
            if (movesUp && gameObject.transform.position.y <= heightPoint.y - 0.1f)
            {                
                gameObject.transform.position = Vector3.SmoothDamp(gameObject.transform.position, heightPoint, ref velocity,  Time.deltaTime * speed);
            }
            else
            {
                movesUp = false;
                gameObject.transform.position = Vector3.SmoothDamp(gameObject.transform.position, startPos, ref velocity,  Time.deltaTime * speed);
                
                if (gameObject.transform.position.y <= startPos.y + 0.1f)
                {
                    movesUp = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
