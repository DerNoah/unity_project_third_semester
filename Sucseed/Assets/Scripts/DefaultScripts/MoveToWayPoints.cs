﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToWayPoints : MonoBehaviour
{
    private List<GameObject> wayPoints = new List<GameObject>();
    public float speed;
    public float maxDistanceToWayPoint = 0.25f;


    void Awake()
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("WayPoint");

        foreach (GameObject item in gameObjects)
        {
            if (item.name.Contains(this.transform.name))
            {
                wayPoints.Add(item);
            }
        }

        if (wayPoints.Count > 0)
        {
            this.gameObject.transform.position = wayPoints[Random.Range(0, wayPoints.Count)].transform.position;
            StartCoroutine(WalkWayPoints(this.gameObject, wayPoints));
        }
    }


    private IEnumerator WalkWayPoints(GameObject gameObject, List<GameObject> wayPoints)
    {
        Vector3 velocity = Vector3.zero;
        GameObject nextWayPoint = wayPoints[0];
        
        while (true)
        {
            if (Vector3.Distance(gameObject.transform.position, nextWayPoint.transform.position) >= maxDistanceToWayPoint)
            {
                gameObject.transform.position = Vector3.SmoothDamp(gameObject.transform.position, nextWayPoint.transform.position, ref velocity, Time.deltaTime * speed);
            }
            else
            {
                if (gameObject.name.Contains("Ballon"))
                {
                    yield return new WaitForSeconds(30);
                }
                nextWayPoint = wayPoints[Random.Range(0, wayPoints.Count)];
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
