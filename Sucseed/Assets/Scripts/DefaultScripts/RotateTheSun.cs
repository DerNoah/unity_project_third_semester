﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTheSun : MonoBehaviour
{
    private IEnumerator sunRoutine;


    private void Awake()
    {
        StartCoroutine(RotateSun());
    }


    private IEnumerator RotateSun()
    {
        float speed;

        while (true)
        {
            if (this.transform.rotation.eulerAngles.z >= 85 && this.transform.rotation.eulerAngles.z <= 275)
            {
                speed = 15;
            }
            else
            {
                speed = 0.5f;
            }
            transform.Rotate(new Vector3(0, 0, 1) * (speed * Time.deltaTime));

            yield return speed;
        }
    }
}
