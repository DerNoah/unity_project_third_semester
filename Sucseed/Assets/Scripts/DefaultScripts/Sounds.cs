﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Sounds : MonoBehaviour
{
    public void Start()
    {
        BackgroundSound();
    }

    void BackgroundSound()
    {
        AudioSource backgroundM = gameObject.AddComponent<AudioSource>();

        if (this.transform.name == "Logic")
        {
            backgroundM.clip = (AudioClip)Resources.Load("Audio/Shards_of_a_world-Ambiance");
        }
        else
        {
            backgroundM.clip = (AudioClip)Resources.Load("Audio/Wind-Mark_DiAngelo-1940285615");
            backgroundM.volume = 0.1f;
        }
        backgroundM.loop = enabled;
        backgroundM.Play();
    }
}
