﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExtensionMethods;

public class ClickOnField : MonoBehaviour
{
    void Update()
    {
        //Set Cursor on rightClick to normal
        if (CursorScript.cursorState != CursorStates.Normal && Input.GetMouseButtonDown(1))
        {
            CursorScript.cursorState = CursorStates.Normal;
        }

        //Shows wich field was Clicked
        SetFrameToClickedField(SelectedField());

        //Add Fields to be edited in Fieldgrid script and Edit it by and by
        if (Input.GetMouseButtonDown(0) && SelectedField() != null && CursorScript.cursorState != CursorStates.Normal)
        {
            Debug.Log("Feld " + SelectedField().fieldNumber[0] + " " + SelectedField().fieldNumber[1] + " wurde hinzugefügt");

            GetComponent<FieldGrid>().ToBeEditedField = SelectedField();
        }
    }

    #region Move a frame to clicked field for an optic feedback
    private GameObject chooseFrame;
    private void SetFrameToClickedField(Field field)
    {
        if (Input.GetMouseButtonDown(0) && !Inventur.InvCanvasActive)
        {
            if (CursorScript.cursorState != CursorStates.Normal && SelectedField() == null)
            {
                CursorScript.cursorState = CursorStates.Normal;
            }

            if (chooseFrame != null && field != null)
            {
                chooseFrame.transform.position = field.fieldObject.transform.position;
            }
            else if (field != null && chooseFrame == null)
            {
                chooseFrame = Instantiate(Resources.Load<GameObject>("Prefabs/Frame"));
                chooseFrame.transform.position = field.worldPosition;
            }
            else if (field == null && chooseFrame != null)
            {
                Destroy(chooseFrame);
            }
        }
        else if (Inventur.InvCanvasActive)
        {
            Destroy(chooseFrame);
        }
    }
    #endregion

    #region Get Field that was Clicked
    public Field SelectedField()
    {
        Field returnValue = null;
        GameObject hittedGameObject = null;

        Camera mainCamera = FindObjectOfType<Camera>();

        Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;

        if (Physics.Raycast(mouseRay, out raycastHit))
        {
            hittedGameObject = raycastHit.transform.gameObject;

        }
        else
        {
            returnValue = null;
        }
        for (int i = 0; i < FieldGrid.fields.GetUpperBound(0) + 1; i++)
        {
            bool boolToBreak = false;
            for (int j = 0; j < FieldGrid.fields.GetUpperBound(1) + 1; j++)
            {
                if (FieldGrid.fields[i, j].fieldObject == hittedGameObject)
                {
                    boolToBreak = true;
                    returnValue = FieldGrid.fields[i, j];
                    break;
                }
            }
            if (boolToBreak)
            {
                break;
            }
        }
        return returnValue;
    }
    #endregion
}