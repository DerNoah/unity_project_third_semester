﻿//Noah Plützer
using System.Collections;
using UnityEngine;


public enum StateOfField
{
    Default,
    Watered,
    Hooked,
    WateredAndHooked
}


public class Field
{
    public GameObject fieldObject;
    public int[] fieldNumber = new int[2];
    public Vector3 worldPosition;
    public StateOfField state;
    public Plant plantOnField;
    public CursorStates whatToDoAboutThis;
    const string resourcesPathToDefaultPrefab = "Prefabs/DefaultField";
    const string resourcesPathToHookedPrefab = "Prefabs/HookedField";
    const string resourcesPathToWateredPrefab = "Prefabs/WateredField";
    const string resourcesPathToWateredAndHookedPrefab = "Prefabs/WateredAndHookedField";
    const string resourcesPathToSowedField = "Prefabs/SowedField";
    const string resourcesPathToWateredAndSowedPrefab = "Prefabs/WateredAndSowedField";



    public IEnumerator dryFieldEnumerator()
    {
        while (this.state == StateOfField.WateredAndHooked ||
               this.state == StateOfField.Watered)
        {
            yield return new WaitForSeconds(60);

            if (this.state == StateOfField.Watered)
            {
                this.state = StateOfField.Default;
                this.Check();
            }
            else
            {
                this.state = StateOfField.Hooked;
                this.Check();
            }

        }
    }



    public void Check()
    {
        GameObject defaultField = (GameObject)Resources.Load(resourcesPathToDefaultPrefab);
        GameObject hookedField = (GameObject)Resources.Load(resourcesPathToHookedPrefab);
        GameObject wateredField = (GameObject)Resources.Load(resourcesPathToWateredPrefab);
        GameObject wateredAndHookedPrefab = (GameObject)Resources.Load(resourcesPathToWateredAndHookedPrefab);
        GameObject SowedField = (GameObject)Resources.Load(resourcesPathToSowedField);
        GameObject wateredAndSowedPrefab = (GameObject)Resources.Load(resourcesPathToWateredAndSowedPrefab);
        Vector3 oldScale = fieldObject.transform.localScale;

        if (this.fieldObject.name != this.state.ToString() + "field" || this.fieldObject.name != this.state.ToString() + "field(clone)")
        {
            Object.Destroy(this.fieldObject);
        }

        switch (state)
        {
            case StateOfField.Default:
                {
                    if (plantOnField != null)
                    {
                        if (plantOnField.plantGameObject != null)
                        {
                            Object.Destroy(plantOnField.plantGameObject);
                        }
                    }
                    plantOnField = null;
                    fieldObject = Object.Instantiate(defaultField);
                    break;
                }
            //Macht eigentlich keinen Sinn für das Spiel ein normales Feld zu gießen.
            case StateOfField.Watered:
                {
                    fieldObject = Object.Instantiate(wateredField);
                    break;
                }
            case StateOfField.Hooked:
                {
                    if (plantOnField != null)
                    {
                        fieldObject = Object.Instantiate(SowedField);
                    }
                    else
                    {
                        fieldObject = Object.Instantiate(hookedField);
                    }
                    break;
                }
            case StateOfField.WateredAndHooked:
                {
                    if (plantOnField != null)
                    {
                        fieldObject = Object.Instantiate(wateredAndSowedPrefab);
                    }
                    else
                    {
                        fieldObject = Object.Instantiate(wateredAndHookedPrefab);
                    }
                    break;
                }
        }

        fieldObject.transform.position = worldPosition;
        fieldObject.transform.localScale = oldScale;

        fieldObject.AddComponent<CoroutineStarter>();

        this.fieldObject.GetComponent<CoroutineStarter>().fieldOfThisGameObject = this;

        Debug.Log("Field was checked");
    }
}