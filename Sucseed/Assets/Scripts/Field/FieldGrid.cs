﻿//Noah Plützer
using System.Collections.Generic;
using UnityEngine;

public class FieldGrid : MonoBehaviour
{
    public static Field[,] fields;

    #region Fields that will be edited
    private List<Field> fieldsToBeHooked = new List<Field>();
    private List<Field> fieldsToBeWatered = new List<Field>();
    private List<Field> fieldsToBeDiged = new List<Field>();
    private List<Field> fieldsToBeHarvest = new List<Field>();
    private Field nextFieldToBeSowed;
    #endregion


    public Field ToBeEditedField
    {
        get
        {
            if (fieldsToBeHooked.Count > 0)
            {
                fieldsToBeHooked[0].whatToDoAboutThis = CursorStates.Hoe;
                return fieldsToBeHooked[0];
            }
            else if (fieldsToBeWatered.Count > 0)
            {
                fieldsToBeWatered[0].whatToDoAboutThis = CursorStates.WateringCan;
                return fieldsToBeWatered[0];
            }
            else if (fieldsToBeDiged.Count > 0)
            {
                fieldsToBeDiged[0].whatToDoAboutThis = CursorStates.Shovel;
                return fieldsToBeDiged[0];
            }
            else if (fieldsToBeHarvest.Count > 0)
            {
                fieldsToBeHarvest[0].whatToDoAboutThis = CursorStates.Sickle;
                return fieldsToBeHarvest[0];
            }
            else if (nextFieldToBeSowed != null)
            {
                nextFieldToBeSowed.whatToDoAboutThis = CursorStates.Seed;
                CursorScript.cursorState = CursorStates.Normal;
                return nextFieldToBeSowed;
            }
            else
            {
                return null;
            }
        }
        set
        {
            switch (CursorScript.cursorState)
            {
                case CursorStates.Hoe:
                    {
                        if (!fieldsToBeHooked.Contains(value) && value.state != StateOfField.Hooked)
                        {
                            value.whatToDoAboutThis = CursorScript.cursorState;
                            fieldsToBeHooked.Add(value);
                        }
                        break;
                    }
                case CursorStates.WateringCan:
                    {
                        if (!fieldsToBeWatered.Contains(value) && (value.state != StateOfField.Watered || value.state != StateOfField.WateredAndHooked))
                        {
                            value.whatToDoAboutThis = CursorScript.cursorState;
                            fieldsToBeWatered.Add(value);
                        }
                        break;
                    }
                case CursorStates.Shovel:
                    {
                        if (!fieldsToBeDiged.Contains(value) && value.state != StateOfField.Default)
                        {
                            value.whatToDoAboutThis = CursorScript.cursorState;
                            fieldsToBeDiged.Add(value);
                        }
                        break;
                    }
                case CursorStates.Sickle:
                    {
                        if (!fieldsToBeHarvest.Contains(value) && value.plantOnField != null && value.plantOnField.stateOfPlant == PlantState.Flower)
                        {
                            value.whatToDoAboutThis = CursorScript.cursorState;
                            fieldsToBeHarvest.Add(value);
                        }
                        break;
                    }
                case CursorStates.Seed:
                    {
                        if (value.plantOnField == null)
                        {
                            value.whatToDoAboutThis = CursorScript.cursorState;
                            nextFieldToBeSowed = value;
                        }
                        break;
                    }
            }
        }
    }

    #region initialize
    private void Awake()
    {
        fields = new Field[7, 5];

        for (int i = 0; i < fields.GetUpperBound(0) + 1; i++)
        {
            for (int j = 0; j < fields.GetUpperBound(1) + 1; j++)
            {
                fields[i, j] = new Field();
                fields[i, j].fieldNumber[0] = i;
                fields[i, j].fieldNumber[1] = j;
                fields[i, j].state = StateOfField.Default;

                fields[i, j].fieldObject = Instantiate((GameObject)Resources.Load("Prefabs/DefaultField"));

                fields[i, j].fieldObject.transform.localScale = new Vector3(2, 1, 2);
                fields[i, j].worldPosition = new Vector3(i * fields[i, j].fieldObject.transform.localScale.x, 0, j * fields[i, j].fieldObject.transform.localScale.z);
                fields[i, j].fieldObject.transform.position = fields[i, j].worldPosition;

                fields[i, j].Check();
            }
        }
    }
    #endregion

    private void Update()
    {
        if (ToBeEditedField != null && CreatePlayer.player.activeCoroutine == null)
        {
            Debug.Log("Als nächstes " + ToBeEditedField.fieldNumber[0] + " " + ToBeEditedField.fieldNumber[1]);

            CreatePlayer.player.activeCoroutine = StartCoroutine(CreatePlayer.player.MoveToAndEditField(ToBeEditedField));
        }
    }


    //Removes the field that was edited out the Lists
    public void FieldWasEdited()
    {
        if (fieldsToBeHooked.Count > 0)
        {
            fieldsToBeHooked.Remove(ToBeEditedField);
        }
        else if (fieldsToBeWatered.Count > 0)
        {
            fieldsToBeWatered.Remove(ToBeEditedField);
        }
        else if (fieldsToBeDiged.Count > 0)
        {
            fieldsToBeDiged.Remove(ToBeEditedField);
        }
        else if (fieldsToBeHarvest.Count > 0)
        {
            fieldsToBeHarvest.Remove(ToBeEditedField);
        }
        else if (nextFieldToBeSowed != null)
        {
            nextFieldToBeSowed = null;
        }
    }
}