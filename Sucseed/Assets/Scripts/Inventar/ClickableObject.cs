﻿using UnityEngine;
using UnityEngine.EventSystems;
/// <summary>
/// Made by Hannes Bauschke 
/// + Unity Doccumentation + Noah Plützer Heritage
/// </summary>
public class ClickableObject : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            //Debug.Log("Left click");
        }

        else if (eventData.button == PointerEventData.InputButton.Middle)
        {
            //Debug.Log("Middle click");
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            this.gameObject.GetComponent<Slot>().RightClickMenu();
        }

    }
}
