﻿using UnityEngine;
/// <summary>
/// Made by Hannes Bauschke
/// </summary>
public class Craft : MonoBehaviour
{
    public GameObject Mother;
    int P1, P2, P3, P12, P23, P13;
    /// <summary>
    /// Die Generelle Benötigte Anzal an Items die man von jeweils braucht um ein neues item herzustellen
    /// </summary>
    public int GBAI;
    /// <summary>
    /// Die Generelle Benötigte Anzal an Items die man von dem selben braucht um ein gleiches item herzustellen
    /// </summary>
    public int GBAIS;
    /// <summary>
    /// Die Generelle Anzal an Items die aus der Herstellung eines neuen items resultiern
    /// </summary>
    public int GRAI;
    /// <summary>
    /// Die Generelle Anzal an Items die aus der Herstellung eines gleichen items resultiern
    /// </summary>
    public int GRAIS;
    public void Crafting()
    {
        foreach (var slot in Inventur.slots)
        {
            if (slot.GetComponent<Slot>().SlotItem == Inventur.PCP_P1)
            {
                P1++;
                slot.GetComponent<Slot>().SlotItem = Inventur.PCP_EmptySlot;
            }
            if (slot.GetComponent<Slot>().SlotItem == Inventur.PCP_P2)
            {
                P2++;
                slot.GetComponent<Slot>().SlotItem = Inventur.PCP_EmptySlot;
            }
            if (slot.GetComponent<Slot>().SlotItem == Inventur.PCP_P3)
            {
                P3++;
                slot.GetComponent<Slot>().SlotItem = Inventur.PCP_EmptySlot;
            }
            if (slot.GetComponent<Slot>().SlotItem == Inventur.PCP_P12)
            {
                P12++;
                slot.GetComponent<Slot>().SlotItem = Inventur.PCP_EmptySlot;
            }
            if (slot.GetComponent<Slot>().SlotItem == Inventur.PCP_P23)
            {
                P23++;
                slot.GetComponent<Slot>().SlotItem = Inventur.PCP_EmptySlot;
            }
            if (slot.GetComponent<Slot>().SlotItem == Inventur.PCP_P13)
            {
                P13++;
                slot.GetComponent<Slot>().SlotItem = Inventur.PCP_EmptySlot;
            }
        }
        //Combine Normal
        Combine(P1, GBAI, P2, GBAI, Inventur.PCP_P12, GRAI);
        Combine(P2, GBAI, P3, GBAI, Inventur.PCP_P23, GRAI);
        Combine(P1, GBAI, P3, GBAI, Inventur.PCP_P13, GRAI);
        Combine(P12, GBAI, P13, GBAI, Inventur.PCP_P123, GRAI);
        Combine(P23, GBAI, P13, GBAI, Inventur.PCP_P123, GRAI);
        Combine(P23, GBAI, P12, GBAI, Inventur.PCP_P123, GRAI);
        //Combine Selbes
        Combine(P1, GBAIS, P1, GBAIS, Inventur.PCP_P1, GRAIS);
        Combine(P2, GBAIS, P2, GBAIS, Inventur.PCP_P2, GRAIS);
        Combine(P3, GBAIS, P3, GBAIS, Inventur.PCP_P3, GRAIS);
        Combine(P12, GBAIS, P12, GBAIS, Inventur.PCP_P12, GRAIS);
        Combine(P13, GBAIS, P13, GBAIS, Inventur.PCP_P13, GRAIS);
        Combine(P23, GBAIS, P23, GBAIS, Inventur.PCP_P23, GRAIS);
        /*
        //1-Fach
        Combine(P4, GBAIS, P4, GBAIS, "P4", GRAIS);
        Combine(P5, GBAIS, P5, GBAIS, "P5", GRAIS);
        //2-Fach
        Combine(P14, GBAIS, P14, GBAIS, "P14", GRAIS);
        Combine(P15, GBAIS, P15, GBAIS, "P15", GRAIS);
        Combine(P24, GBAIS, P24, GBAIS, "P24", GRAIS);
        Combine(P25, GBAIS, P25, GBAIS, "P25", GRAIS);
        Combine(P34, GBAIS, P34, GBAIS, "P34", GRAIS);
        Combine(P35, GBAIS, P35, GBAIS, "P35", GRAIS);
        Combine(P45, GBAIS, P45, GBAIS, "P45", GRAIS);
        */
        
        P1 = 0;
        P2 = 0;
        P3 = 0;
        P12 = 0;
        P23 = 0;
        P13 = 0;

    }
    void Combine(int ItemVariableA, int ItemNumberNeedA, int ItemVariableB, int ItemNumberNeedB, Item ItemResult, int ItemResultNumber)
    {
        if (ItemVariableA >= ItemNumberNeedA && ItemVariableB >= ItemNumberNeedB)
        {
            Mother.GetComponent<Inventur>().AddItemToInv(ItemResultNumber, ItemResult);

            ItemVariableA -= ItemNumberNeedA;
            ItemVariableB -= ItemNumberNeedB;
        }
    }
}