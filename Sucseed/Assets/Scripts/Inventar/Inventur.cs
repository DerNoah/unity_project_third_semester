﻿using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Made by Hannes Bauschke
/// </summary>
public class Inventur : MonoBehaviour
{
    public static Inventur Inventar;
    public static Button[,] Slots;
    public GameObject InvCanvas;
    public int Invtiefe, Invbreite;
    public GameObject WorkbankCanvas;
    public int tiefe, breite;
    bool WorkbankCanvasActive = false;
    static Button[,] InvSlots;
    public static bool InvCanvasActive = false;
    public Button Slot;
    public Button CraftB;
    GameObject selectedItem;

        
    public static Item PCP_EmptySlot;
    public static Item PCP_P1;
    public static Item PCP_P2;
    public static Item PCP_P3;
    public static Item PCP_P12;
    public static Item PCP_P13;
    public static Item PCP_P23;
    public static Item PCP_P123;
    private void Awake()
    {
        PCP_EmptySlot = new Item("Empty Slot", Resources.Load<Sprite>("Sprites/EmptySlot"), typeof(EmptySlot),0);
        PCP_P1 = new Item("P1", Resources.Load<Sprite>("Sprites/Planze1"), typeof(BasePlant1),1);
        PCP_P2 = new Item("P2", Resources.Load<Sprite>("Sprites/Planze2"), typeof(BasePlant2),4);
        PCP_P3 = new Item("P3", Resources.Load<Sprite>("Sprites/Planze3"), typeof(BasePlant3),6);
        PCP_P12 = new Item("P12", Resources.Load<Sprite>("Sprites/Planze12"), typeof(CombinatedPlant1and2),8);
        PCP_P13 = new Item("P3", Resources.Load<Sprite>("Sprites/Planze13"), typeof(CombinatedPlant1and3),12);
        PCP_P23 = new Item("P23", Resources.Load<Sprite>("Sprites/Planze23"), typeof(CombinatedPlant2and3),15);
        PCP_P123 = new Item("P123", Resources.Load<Sprite>("Sprites/Planze123"), typeof(CombinatedPlant1and2and3),20);
}
    public static Button[,] slots
    {
        get { return Slots; }
        set { Slots = value; Debug.Log(Slots + " wird verändert"); }
    }

    void Start()
    {
        Inventar = this;
        Slots = new Button[tiefe, breite];
        for (int i = 0; i < tiefe; i++)
        {
            for (int b = 0; b < breite; b++)
            {
                Button Temp = Instantiate(Slot, WorkbankCanvas.transform);
                Temp.gameObject.transform.position = new Vector3(50 * (i + 1), 50 * (b + 1), 0);
                Temp.GetComponent<Slot>().Mutter = this.gameObject;
                Slots[i, b] = Temp;
                if (i + b == tiefe + breite - 2)
                {
                    Button CTemp = Instantiate(CraftB, WorkbankCanvas.transform);
                    CTemp.gameObject.transform.position = new Vector3(50 * (i + 2), 50 * (b + 1), 0);
                    CTemp.GetComponent<Craft>().Mother = this.gameObject;
                }
            }
        }
        WorkbankCanvas.SetActive(false);

        InvSlots = new Button[Invtiefe, Invbreite];
        for (int i = 0; i < Invtiefe; i++)
        {
            for (int b = 0; b < Invbreite; b++)
            {
                Button Temp = Instantiate(Slot, InvCanvas.transform);
                Temp.gameObject.transform.position = new Vector3(50 * (i + 10), 50 * (b + 3), 0);
                Temp.GetComponent<Slot>().Mutter = this.gameObject;
                InvSlots[i, b] = Temp;
            }
        }

        InvCanvas.SetActive(false);
        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            OpenWorkbank();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            ChangeInv();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            InvCanvas.SetActive(false);
            Shop.ShopCV.SetActive(false);
            WorkbankCanvas.SetActive(false);
            MoneyManager.GoldCv.SetActive(!MoneyManager.GoldCv.activeSelf);
            CursorScript.cursorScript.GetComponent<CursorScript>().csHeader.SetActive(!CursorScript.cursorScript.GetComponent<CursorScript>().csHeader.activeSelf);
        }
    }
    public void AddItemToInv(int numberOfAddedItems, Item AddedItem)
    {
        Debug.Log("Anfängliche Anzahl zu addender Items " + numberOfAddedItems);
        for (int b = 0; b < Invbreite; b++)
        {
            for (int i = 0; i < Invtiefe; i++)
            {
                if (InvSlots[i, b].GetComponent<Slot>().SlotItem.GetName() == "Empty Slot" && numberOfAddedItems > 0)
                {
                    InvSlots[i, b].GetComponent<Slot>().SlotItem = AddedItem;
                    numberOfAddedItems--;
                    Debug.Log("Item " + (i * (b + 1)) + " wurde hinzugefügt");
                    Debug.Log("Item " + numberOfAddedItems + " wurde hinzugefügt");
                }

            }
        }
    }
    public void OpenWorkbank()
    {
        WorkbankCanvasActive = !WorkbankCanvas.activeSelf;
        WorkbankCanvas.SetActive(WorkbankCanvasActive);
    }
    public void ChangeInv()
    {
        InvCanvasActive = !InvCanvas.activeSelf;
        InvCanvas.SetActive(InvCanvasActive);
        CursorScript.cursorScript.GetComponent<CursorScript>().csHeader.SetActive(!InvCanvasActive);
    }

    public void OpenInv()
    {
        InvCanvas.SetActive(true);
        CursorScript.cursorScript.GetComponent<CursorScript>().csHeader.SetActive(false);
    }
    public void CloseInv()
    {
        InvCanvas.SetActive(false);
        CursorScript.cursorScript.GetComponent<CursorScript>().csHeader.SetActive(true);
    }
    public void SetSelectItem(GameObject SelectedItem)
    {
        selectedItem = SelectedItem;
    }
    public GameObject GetSelectItem()
    {
        return selectedItem;
    }
    public GameObject GetMother()
    {
        return this.gameObject;
    }
    void StartItems()
    {
        //Start Item
        OpenInv();
        Invoke("SetStartItems", 0.001f);
        Invoke("OpenInv", 0.001f);
    }
    public void SetStartItems()
    {
        AddItemToInv(1, PCP_P1);
    }
}
