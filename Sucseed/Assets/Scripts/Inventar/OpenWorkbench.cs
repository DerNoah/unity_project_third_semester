﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Made by Hannes Bauschke
/// </summary>
public class OpenWorkbench : MonoBehaviour {

    void OnMouseDown()
    {
        Inventur.Inventar.OpenWorkbank();
    }
}
