﻿using UnityEngine;
using UnityEngine.UI;
using System;
/// <summary>
/// Made by Hannes Bauschke
/// </summary>
public class Slot : MonoBehaviour
{
    private Item slotItem;
    [SerializeField]
    public Item SlotItem
    {
        get { return slotItem; }
        set
        {
            slotItem = value;
            SlotItem.SetParent(this.gameObject);
        }
    }
    public GameObject Mutter,TochterMenu;
    void Start()
    {
        slotItem = new Item("Empty Slot", Resources.Load<Sprite>("Sprites/EmptySlot"), typeof(EmptySlot),0);

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }
    }
    public void ItemSwap()
    {
        if (Mutter.GetComponent<Inventur>().GetSelectItem() != null)
        {
            Item temp = slotItem;
            Debug.Log(temp.GetName() + " is the temp");
            SlotItem = Mutter.GetComponent<Inventur>().GetSelectItem().GetComponent<Slot>().slotItem;
            Mutter.GetComponent<Inventur>().GetSelectItem().GetComponent<Slot>().SlotItem = temp;
            Debug.Log("Swapped");
            Mutter.GetComponent<Inventur>().SetSelectItem(null);
        }
        else
        {
            Mutter.GetComponent<Inventur>().SetSelectItem(this.gameObject);
        }
    }
    public void RightClickMenu()
    {
        if (slotItem.GetName() != "Empty Slot")
        {

        TochterMenu.SetActive(!TochterMenu.activeSelf);
        }
    }
    public void Auswählen()
    {
        //Item Übergeben 
        slotItem.SetParent(this.gameObject);
        CreatePlayer.player.heltedSeed.Add(slotItem);
        //Cursor wechseln
        CursorScript.cursorState = CursorStates.Seed;
        //Untermenu schließen
        Exit();
        //Schliest das Inventar
        Mutter.GetComponent<Inventur>().CloseInv();
        Shop.ShopCV.SetActive(false);
    }
    public void Wegwerfen()
    {
        //Slot Leer machen
        SlotItem = Inventur.PCP_EmptySlot;
        //Cursor resetten
        CursorScript.cursorState = CursorStates.Normal;
        //Untermenu schließen
        Exit();
    }
    public void Verkaufen()
    {
        //Wert rausholen
        int money = SlotItem.value;
        //Slot Leer machen
        SlotItem = Inventur.PCP_EmptySlot;
        //Cursor resetten
        CursorScript.cursorState = CursorStates.Normal;
        //Geld ausgeben
        MoneyManager.instance.MoneyChange(money);

        MyMoneyText.LoseMoney.gameObject.SetActive(true);
        MyMoneyText.losemoney= money;
        MyMoneyText.LoseMoney.GetComponent<Text>().color = Color.green;
        //Untermenu schließen
        Exit();
    }

    public void Exit()
    {
        TochterMenu.SetActive(false);
    }
}
public class Item 
{
    string name;
    Sprite itemSprite;
    public GameObject parent;
    public Type plantType;
    public int value;
    
    public Item(string Name, Sprite ItemSprite, Type PlantType, int _Value)
    {
        name = Name;
        plantType = PlantType;
        itemSprite = ItemSprite;
        value = _Value;
    }
    
    public void SetName(string Name)
    {
        name = Name;

    }
    public void SetParent(GameObject Parent)
    {
        parent = Parent;
        Debug.Log(itemSprite);
        SetSprite();

    }
    public void SetSprite()
    {
        parent.GetComponent<Image>().sprite = itemSprite;

    }
    public string GetName()
    {
        return name;

    }
}