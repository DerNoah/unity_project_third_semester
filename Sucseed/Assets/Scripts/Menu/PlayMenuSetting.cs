﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayMenuSetting : MonoBehaviour {
    [System.Obsolete]
    public void StartScene()
    {
        SceneManager.LoadScene("CharChoose");
    }

    public void CreditScene()
    {
        SceneManager.LoadScene("Credits");
    }


    public void MenuScene()
    {
        SceneManager.LoadScene("Menu");
    }

    public void EndScene()
    {
        Application.Quit();
    }

}
