﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MyMoneyText : MonoBehaviour {

    public static Text Money;
    public static Text LoseMoney;
    public static Text GetMoney;
    

    public static int losemoney;
    public static int mymoney;

    string plus = "+";

    private void Start()
    {
       
       LoseMoney.gameObject.SetActive(false);
    }

    private void Update()
    {
        Money.text = MoneyManager.myMoney.ToString();
       // LoseMoney.text = losemoney.ToString();

        if (LoseMoney.color == Color.green)
        {
            LoseMoney.text = plus + losemoney.ToString();
            StartCoroutine(Fade());
        }

        if (LoseMoney.color == Color.red)
        {
            LoseMoney.text = losemoney.ToString();
        }


    }
    public IEnumerator Fade()
    {
        yield return new WaitForSeconds(0.4f);
        MyMoneyText.LoseMoney.color = Color.clear;
    }
}
