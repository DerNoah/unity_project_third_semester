﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MoneyManager : MonoBehaviour {

    public static int myMoney = 200;

    public static MoneyManager instance;


    public static GameObject GoldCv;



    void Awake()
    {
        instance = this;
    }

    public void MoneyChange(int number)
    {
        myMoney = myMoney + number;

        if (myMoney <= 0)
        {
            myMoney = 0;
        }
    }
    

}
