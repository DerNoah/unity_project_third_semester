﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Pause : MonoBehaviour
{

    public GameObject BackGroundPause;

    public GameObject[] PauseObjects;
    public static GameObject Shoping;

    bool GamePaused = false;
    public static GameObject inventarlist;

    void Start()
    {
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (GamePaused)
            {
                ResumeThisGame();
            }
            else
            {
                PauseThisGame();
            }

        }
    }
    
    public void PauseThisGame()
    {
        BackGroundPause.SetActive(true);
        Shoping.SetActive(false);
        PauseObjects[0].SetActive(false);
        inventarlist.GetComponent<Inventur>().enabled = false;
        PauseObjects[1].SetActive(false);
        PauseObjects[2].SetActive(false);
        PauseObjects[3].SetActive(false);
        PauseObjects[4].SetActive(false);
        PauseObjects[5].SetActive(false);
        Time.timeScale = 0;
        GamePaused = true;
    }

    public void ResumeThisGame()
    {
        BackGroundPause.SetActive(false);
        Shoping.SetActive(true);
       // PauseObjects[0].SetActive(true);
        inventarlist.GetComponent<Inventur>().enabled = true;
        PauseObjects[1].SetActive(true);
        PauseObjects[2].SetActive(true);
        PauseObjects[3].SetActive(true);
        PauseObjects[4].SetActive(true);
        PauseObjects[5].SetActive(true);
        Time.timeScale = 1f;
        GamePaused = false;
    }

    public void BackToMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }
}
