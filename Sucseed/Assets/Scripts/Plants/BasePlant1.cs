﻿//Noah Plützer
using System.Collections;
using UnityEngine;
using ExtensionMethods;


public class BasePlant1 : Plant
{
    private string _pathToSpecific_S_Size = "Prefabs/Radish_S";
    private string _pathToSpecific_M_Size = "Prefabs/Radish_M";
    private string _pathToSpecfiic_L_Size = "Prefabs/Radish_L";
    public GameObject plantGameObject;
    public Vector3 scenePositionOfPlant;
    public int[] fieldNumber = new int[2];
    public PlantState stateOfPlant;
    public Item thisplantItem = Inventur.PCP_P1;
    public int growingRateInSeconds;


    #region get and set for Plant interface
    Item Plant.thisPlantsItem { get { return this.thisplantItem; } }
    string Plant.pathToSpecific_S_Size { get { return this._pathToSpecific_S_Size; } }
    string Plant.pathToSpecific_M_Size { get { return this._pathToSpecific_M_Size; } }
    string Plant.pathToSpecific_L_Size { get { return this._pathToSpecfiic_L_Size; } }
    GameObject Plant.plantGameObject
    {
        get { return this.plantGameObject; }
        set { this.plantGameObject = value; }
    }
    Vector3 Plant.scenePositionOfPlant
    {
        get { return this.scenePositionOfPlant; }
        set { this.scenePositionOfPlant = value; }
    }
    int[] Plant.fieldNumber
    {
        get { return this.fieldNumber; }
        set { this.fieldNumber = value; }
    }
    PlantState Plant.stateOfPlant
    {
        get { return this.stateOfPlant; }
        set { this.stateOfPlant = value; }
    }
    IEnumerator Plant.enumerator
    {
        get { return this.GrowAfterTime(); }
    }
    #endregion


    IEnumerator GrowAfterTime()
    {
        int tempInt = 0;

        while (this.stateOfPlant != PlantState.Flower && FieldGrid.fields[fieldNumber[0], fieldNumber[1]].state == StateOfField.WateredAndHooked)
        {
            if (this.stateOfPlant == PlantState.Seed && tempInt == 0)
            {
                yield return new WaitForSeconds(growingRateInSeconds);
                tempInt++;
            }

            this.Grow();

            this.CheckPlant();
            yield return new WaitForSeconds(growingRateInSeconds);
        }
    }



    //Constructor
    public BasePlant1(int[] fieldNumber)
    {
        this.fieldNumber = fieldNumber;
        this.stateOfPlant = PlantState.Seed;
        this.growingRateInSeconds = 30;

        this.plantGameObject = Object.Instantiate(Resources.Load<GameObject>(_pathToSpecific_S_Size));

        scenePositionOfPlant = new Vector3((FieldGrid.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.position.x),              // position in Unity Scene x                                          
                                           (FieldGrid.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.position.y) +             //}
                                           (FieldGrid.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.localScale.y / 2) +       // position in Unity Scene y
                                           (plantGameObject.transform.localScale.y / 2),                                                     //}
                                           (FieldGrid.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.position.z));             // position in Unity Scene z

        this.plantGameObject.transform.position = scenePositionOfPlant;
        this.plantGameObject.AddComponent<CoroutineStarter>();

        this.plantGameObject.GetComponent<CoroutineStarter>().plantOfThisGameObject = this;
    }
}

