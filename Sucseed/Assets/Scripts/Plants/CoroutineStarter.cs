﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineStarter : MonoBehaviour
{
    public Plant plantOfThisGameObject;
    public Field fieldOfThisGameObject;


    void Start()
    {

        Invoke("GetStarted", 10);
    }

    void GetStarted()
    {
        if (plantOfThisGameObject != null)
        {
            StartCoroutine(plantOfThisGameObject.enumerator);
        }
        else if (fieldOfThisGameObject != null)
        {
            StartCoroutine(fieldOfThisGameObject.dryFieldEnumerator());
        }
    }
}
