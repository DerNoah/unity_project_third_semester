﻿using System.Collections;
using UnityEngine;
using ExtensionMethods;



public class EmptySlot : Plant
{
    private string pathToSpecific_S_Size = "";
    private string pathToSpecific_M_Size = "";
    private string pathToSpecfiic_L_Size = "";
    public GameObject plantGameObject;
    public Vector3 scenePositionOfPlant;
    public int[] fieldNumber = new int[2];
    public PlantState stateOfPlant;
    public Item thisplantItem = Inventur.PCP_EmptySlot;
    public int growingRateInMinutes;


    #region get and set for Plant interface
    Item Plant.thisPlantsItem { get { return this.thisplantItem; } }

    string Plant.pathToSpecific_S_Size
    {
        get
        {
            if (Resources.Load(this.pathToSpecific_S_Size) != null)
            {
                return this.pathToSpecific_S_Size;
            }
            else
            {
                return "Prefabs/Seed";
            }
        }
    }

    string Plant.pathToSpecific_M_Size
    {
        get
        {
            if (Resources.Load(this.pathToSpecific_M_Size) != null)
            {
                return this.pathToSpecific_M_Size;
            }
            else
            {
                return "Prefabs/Cutting";
            }
        }
    }

    string Plant.pathToSpecific_L_Size
    {
        get
        {
            if (Resources.Load(this.pathToSpecfiic_L_Size) != null)
            {
                return this.pathToSpecfiic_L_Size;
            }
            else
            {
                return "Prefabs/Cutting";
            }
        }
    }

    GameObject Plant.plantGameObject
    {
        get { return this.plantGameObject; }
        set { this.plantGameObject = value; }
    }

    Vector3 Plant.scenePositionOfPlant
    {
        get { return this.scenePositionOfPlant; }
        set { this.scenePositionOfPlant = value; }
    }

    int[] Plant.fieldNumber
    {
        get { return this.fieldNumber; }
        set { this.fieldNumber = value; }
    }

    PlantState Plant.stateOfPlant
    {
        get { return this.stateOfPlant; }
        set { this.stateOfPlant = value; }
    }

    IEnumerator Plant.enumerator
    {
        get { return this.GrowAfterTime(); }
    }
    #endregion



    IEnumerator GrowAfterTime()
    {
        while (this.stateOfPlant != PlantState.Flower)
        {
            if (FieldGrid.fields[fieldNumber[0], fieldNumber[1]].state == StateOfField.Watered || 
                FieldGrid.fields[fieldNumber[0], fieldNumber[1]].state == StateOfField.WateredAndHooked)
            {
                this.Grow();

                this.CheckPlant();

                yield return new WaitForSeconds(growingRateInMinutes);
            }
        }
    }



    //Constructor
    public EmptySlot(int[] fieldNumber)
    {
        this.fieldNumber = fieldNumber;
        this.stateOfPlant = PlantState.Seed;

        string resourcesPathToGameObject = "";

        //optional only for debugging
        switch (stateOfPlant)
        {
            case PlantState.Seed:
                resourcesPathToGameObject = pathToSpecific_S_Size;
                break;
            case PlantState.Cutting:
                resourcesPathToGameObject = pathToSpecific_M_Size;
                break;
            case PlantState.Flower:
                resourcesPathToGameObject = pathToSpecfiic_L_Size;
                break;
        }

        this.plantGameObject = Object.Instantiate(Resources.Load<GameObject>(resourcesPathToGameObject));
        /*
        scenePositionOfPlant = new Vector3((CreateGrid.acre.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.position.x),              // position in Unity Scene x                                          
                                           (CreateGrid.acre.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.position.y) +             //}
                                           (CreateGrid.acre.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.localScale.y / 2) +       // position in Unity Scene y
                                           (plantGameObject.transform.localScale.y / 2),                                              //}
                                           (CreateGrid.acre.fields[fieldNumber[0], fieldNumber[1]].fieldObject.transform.position.z));             // position in Unity Scene z
                                           */
        this.plantGameObject.transform.position = scenePositionOfPlant;
    }
}

