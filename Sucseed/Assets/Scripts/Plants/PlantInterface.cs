﻿using UnityEngine;


public enum PlantState
{
    Seed,
    Cutting,
    Flower
}


public interface Plant
{
    Item thisPlantsItem { get; }
    string pathToSpecific_S_Size { get; }
    string pathToSpecific_M_Size { get; }
    string pathToSpecific_L_Size { get; }
    GameObject plantGameObject { get; set; }

    Vector3 scenePositionOfPlant { get; set; }

    int[] fieldNumber { get; set; }                 //x, y

    PlantState stateOfPlant { get; set; }

    System.Collections.IEnumerator enumerator { get; }
}