﻿//Noah Plützer
using UnityEngine;

public interface Charakter
{
    GameObject charakterGameObject { get; set; }
    Vector3 originPosition { get; set; }
}