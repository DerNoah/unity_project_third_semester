﻿//Noah Plützer
using UnityEngine;
using System.IO;


public class CreatePlayer : MonoBehaviour
{
    public static Player player;


    private void Awake()
    {
        player = new Player(new Vector3(15, 0.5f, 3), LoadPlayerFromJson());
    }



    private string LoadPlayerFromJson()
    {
        string returnValue = "";
        string pathToJson = Path.Combine(Application.dataPath, "SaveFiles/ChoosenPlayer.json");
        PlayerData playerName;

        if (File.Exists(pathToJson))
        {
            string jsonFile = File.ReadAllText(pathToJson);
            playerName = JsonUtility.FromJson<PlayerData>(jsonFile);                                          //Fileending (type of prefab)
            string pathToPrefab = Path.Combine(Application.dataPath, "Resources/Prefabs/" + playerName.name + ".prefab");

            if (File.Exists(pathToPrefab))
            {
                returnValue = playerName.name;
            }
        }

        return returnValue;
    }
}
