﻿//Noah Plützer
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ExtensionMethods;

public class Player : Charakter
{
    #region Player memberValues
    public GameObject gameObject;
    public Vector3 originPosition;
    public List<Item> heltedSeed = new List<Item>();
    public Coroutine activeCoroutine = null;
    #endregion

    #region get and set for Charakter Interface
    GameObject Charakter.charakterGameObject
    {
        get { return this.gameObject; }
        set { this.gameObject = value; }
    }
    Vector3 Charakter.originPosition
    {
        get { return this.originPosition; }
        set { this.originPosition = value; }
    }
    #endregion

    public Player(Vector3 startPosition, string wichPrefab)
    {
        this.originPosition = new Vector3(15, 0.5f, 3);
        string resourcesPathToPlayerPrefab = "Prefabs/" + wichPrefab;

        try
        {
            if (File.Exists(Path.Combine(Application.dataPath, "Resources/Prefabs/" + wichPrefab + ".prefab")))
            {
                gameObject = Object.Instantiate(Resources.Load<GameObject>(resourcesPathToPlayerPrefab));
                gameObject.transform.position = startPosition;
            }
            else
            {
                Debug.LogError("Error: Missing PlayerPrefab!");
                gameObject = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                gameObject.transform.position = startPosition;
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e);
        }
    }

    public IEnumerator MoveToAndEditField(Field field)
    {
        while (true)
        {
            if (Vector3.Distance(CreatePlayer.player.gameObject.transform.position, field.fieldObject.transform.position) >= 1)
            {
                gameObject.transform.LookAt(new Vector3(field.fieldObject.transform.position.x, gameObject.transform.position.y, field.fieldObject.transform.position.z));
                gameObject.transform.Translate(gameObject.transform.forward * 5 * Time.deltaTime, Space.World);
            }
            else
            {
                switch (field.whatToDoAboutThis)
                {
                    case CursorStates.Hoe:
                        {
                            yield return new WaitForSeconds(1);
                            field.Hook();
                            break;
                        }
                    case CursorStates.WateringCan:
                        {
                            yield return new WaitForSeconds(1);
                            field.Water();
                            break;
                        }
                    case CursorStates.Shovel:
                        {
                            yield return new WaitForSeconds(1);
                            field.Shovel();
                            break;
                        }
                    case CursorStates.Sickle:
                        {
                            yield return new WaitForSeconds(1);
                            field.Harvest();
                            break;
                        }
                    case CursorStates.Seed:
                        {
                            yield return new WaitForSeconds(1);
                            field.Sow(this);
                            break;
                        }
                }
                GameObject.Find("Logic").GetComponent<FieldGrid>().FieldWasEdited();
                field.whatToDoAboutThis = CursorStates.Normal;
                activeCoroutine = null;
                break;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    public void MoveToOriginPosition()
    {
        if (Vector3.Distance(CreatePlayer.player.gameObject.transform.position, originPosition) >= 0.5f)
        {
            gameObject.transform.LookAt(originPosition);
            gameObject.transform.Translate(gameObject.transform.forward * 5 * Time.deltaTime, Space.World);
        }
        else
        {
            gameObject.transform.LookAt(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - 1));
        }
    }
}