﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Clicked_Shop : MonoBehaviour {

    GameObject Store;

	void Start () {
        Store = Resources.Load<GameObject>("Prefabs/Store");
        Instantiate(Store);
	}
	
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject() && Shop.ShopCV.activeSelf )
            {
                Shop.ShopCV.SetActive(false);
                Inventur.Inventar.CloseInv();
            }
        }
    }


}
