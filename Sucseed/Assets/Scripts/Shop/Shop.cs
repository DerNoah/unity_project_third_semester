﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class Shop : MonoBehaviour {
    
    GraphicRaycaster mouseshoot;
    GameObject Store;
    Text WinorLose;
    public static GameObject ShopCV;
   

    void Awake()
    {
        mouseshoot = GetComponent<GraphicRaycaster>();
        Pause.Shoping = this.gameObject;
        
    }


    void Start () {
        StorePos();
        Shop.ShopCV.SetActive(false);
        WinorLose = Resources.Load<Text>("Prefabs/Store/StoreInv/WinorLose");
    }
 
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            PointerEventData pointData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();
            pointData.position = Input.mousePosition;
            mouseshoot.Raycast(pointData, results);

            foreach (RaycastResult result in results)
            {
                if (result.gameObject.name == "Exit")
                {
                    Shop.ShopCV.SetActive(false);
                    Inventur.Inventar.CloseInv();
                }

                if (result.gameObject.name == "P1" && MoneyManager.myMoney >= 10)
                {
                    Debug.Log(result.gameObject.name);
                    MoneyManager.instance.MoneyChange(-10);
                    Inventur.Inventar.AddItemToInv(1, Inventur.PCP_P1);
                    MyMoneyText.LoseMoney.gameObject.SetActive(true);
                    MyMoneyText.losemoney = -10;
                    MyMoneyText.LoseMoney.GetComponent<Text>().color = Color.red;
                    StartCoroutine(Fade());
                    
                }

                if (result.gameObject.name == "P2" && MoneyManager.myMoney >= 20)
                {
                    Debug.Log(result.gameObject.name);
                    MoneyManager.instance.MoneyChange(-20);
                    Inventur.Inventar.AddItemToInv(1, Inventur.PCP_P2);
                    MyMoneyText.LoseMoney.gameObject.SetActive(true);
                    MyMoneyText.losemoney = -20;
                    MyMoneyText.LoseMoney.GetComponent<Text>().color = Color.red;
                    StartCoroutine(Fade());
                }

                if (result.gameObject.name == "P3" && MoneyManager.myMoney >= 30)
                {
                    Debug.Log(result.gameObject.name);
                    MoneyManager.instance.MoneyChange(-30);
                    Inventur.Inventar.AddItemToInv(1, Inventur.PCP_P3);
                    MyMoneyText.LoseMoney.gameObject.SetActive(true);
                    MyMoneyText.losemoney = -30;
                    MyMoneyText.LoseMoney.GetComponent<Text>().color = Color.red;
                    StartCoroutine(Fade());
                }
            }
        }

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);                
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.collider.gameObject.name == "BallonUpDown" && Shop.ShopCV.activeSelf == false)
                {
                    Shop.ShopCV.SetActive(true);
                    Inventur.Inventar.OpenInv();
                }
            }
        }
    }


    void StorePos()
    {
        Store = GameObject.Find("BallonUpDown");
    }

    public IEnumerator Fade()
    {
        yield return new WaitForSeconds(0.4f);
        MyMoneyText.LoseMoney.color = Color.clear;
    }

}
