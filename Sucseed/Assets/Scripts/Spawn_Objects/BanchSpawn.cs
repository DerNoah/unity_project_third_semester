﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanchSpawn : MonoBehaviour {

    GameObject Bench;
	void Start () {
        BenchPos();
	}
	

    void BenchPos()
    {
        Bench = Resources.Load<GameObject>("Prefabs/bench");
        Bench = Instantiate(Bench);
        Bench.name = "Bench";
        Bench.transform.position = new Vector3(6.74f, 0.42f, 11);
        Bench.AddComponent<BoxCollider>().size = new Vector3(2.43f,0.76f,-0.05f);
        Bench.GetComponent<BoxCollider>().center = new Vector3(-0.02f, 0.76f, -0.05f);
    }
}
