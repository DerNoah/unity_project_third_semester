﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandSpawn : MonoBehaviour {

    GameObject Island;
	void Start () {
        SpawnIsland();
	}


    void SpawnIsland()
    {
        Island = Resources.Load<GameObject>("Prefabs/Insel");
        Island = Instantiate(Island);
        Island.name = "Island";
        Island.transform.position = new Vector3(6.34f, -23.85f, 5.64f);
        Island.transform.localScale = new Vector3(50, 50, 50);
        Island.AddComponent<BoxCollider>().size = new Vector3(13.32f, 0.47f, 14.56f);
        Island.GetComponent<BoxCollider>().center = new Vector3(0, -5.24f, 0);
    }
}
