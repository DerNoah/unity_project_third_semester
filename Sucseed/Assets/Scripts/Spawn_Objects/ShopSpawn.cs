﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSpawn : MonoBehaviour {

    GameObject Store;

    public GameObject StoreInvCv;

    void Start () {
        StorePos();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 50.0f))
            {
                if (hit.transform.name == "Ballon")
                {
                    StoreInvCv.SetActive(true);
                }

            }
        }
    }

    void StorePos()
    {
        Store = Resources.Load<GameObject>("Prefabs/Ballon");
        Store = Instantiate(Store);
        Store.name = "Ballon";
        Store.transform.position = new Vector3(1.5f, 0, 11);
        Store.AddComponent<BoxCollider>().size = new Vector3(4.38f, 7.54f, 2.59f);
        Store.GetComponent<BoxCollider>().center = new Vector3(-0.15f, 3.92f, -0.53f);
    }
}
