﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiloSpawn : MonoBehaviour {

    GameObject Silo;
	void Start () {
        SiloPos();
	}


    void SiloPos()
    {
        Silo = Resources.Load<GameObject>("Prefabs/Silo");
        Silo = Instantiate(Silo);
        Silo.name = "Silo";
        Silo.transform.position = new Vector3(12.35f, 0.54f, 11);
        Silo.transform.Rotate(0,180, 0);
    }
}
