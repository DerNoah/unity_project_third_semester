﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class CraftingBench : MonoBehaviour
{

    GraphicRaycaster OnClickUI;
    public static GameObject Bench;
    bool Crafting = false;
    
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject == Bench && Inventur.Inventar.WorkbankCanvas.activeSelf == false)
                {
                    Inventur.Inventar.OpenWorkbank();
                    Inventur.Inventar.OpenInv();
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
                if (!EventSystem.current.IsPointerOverGameObject() && Inventur.Inventar.WorkbankCanvas.activeSelf)
                {
                    Inventur.Inventar.WorkbankCanvas.SetActive(false);
                    Inventur.Inventar.CloseInv();
                }
            }
            
        }

    }
}

